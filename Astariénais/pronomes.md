# Pronomes (Pronouns)

|                  | Subject     | Accusativ | Dativ / Réflexiv | Genitiv |
|------------------|-------------|-----------|------------------|---------|
| Prim Singular    | Yo          | Mi        | Me               | Mo(s)   |
| Second Singular  | Tu          | Ti        | Te               | To(s)   |
| Tersion Singular | Ile / Ele   | Lo / La   | Se               | So(s)   |
| Prim Plurale     | Nus         | Nus       | Nus              | Nuto(s) |
| Second Plurale   | Vus         | Vus       | Vus              | Vuto(s) |
| Tersion Plurale  | Iles / Eles | Los / Las | Se               | So(s)   |
