# Numeres (Numbers)

| Numeres Arabices | Paraboles de Numeres en le Astarienais |
|------------------|----------------------------------------|
| 1                | Un                                     |
| 2                | Dus                                    |
| 3                | Tres                                   |
| 4                | Quat                                   |
| 5                | Sinque                                 |
| 6                | Ses                                    |
| 7                | Set                                    |
| 8                | Oct                                    |
| 9                | Nob                                    |
| 10               | Dis                                    |
| 11               | Dis-un                                 |
| 12               | Dis-dus                                |
| 13               | Dis-tres                               |
| ...              | ...                                    |
| 20               | Vint                                   |
| 30               | Trent                                  |
| 40               | Quarent                                |
| 50               | Sinquent                               |
| 60               | Sesent                                 |
| 70               | Setent                                 |
| 80               | Octent                                 |
| 90               | Nobent                                 |
| 100              | Un cent                                |
| 101              | Un cent et un                          |
| 110              | Un cent et dis                         |
| 123              | Un cent et vint-tres                   |
| ...              | ...                                    |
| 1000             | Un mile                                |
| 1000000          | Un milion                              |
