# Pronomes (Pronouns)

|                  | Subject     | Accusatib | Datib / Riflexib | Genitib |
|------------------|-------------|-----------|------------------|---------|
| Prim Singular    | Y           | Mi        | Me               | Ma(s)   |
| Second Singular  | Tu          | Ti        | Te               | Ta(s)   |
| Tersion Singular | Ile / Ele   | Lo / La   | Se               | Sa(s)   |
| Prim Plurale     | Nus         | Nus       | Nus              | Na(s)   |
| Second Plurale   | Vus         | Vus       | Vus              | Va(s)   |
| Tersion Plurale  | Iles / Eles | Los / Las | Se               | Sa(s)   |
