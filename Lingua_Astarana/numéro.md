# Numéri (Numbers)

| Numéri Arabici | Paraboli di Numéri en ela Lingua Astarana |
|----------------|-------------------------------------------|
| 1              | Uno/Una                                   |
| 2              | Deus                                      |
| 3              | Tres                                      |
| 4              | Quato                                     |
| 5              | Cinque                                    |
| 6              | Séx                                       |
| 7              | Septe                                     |
| 8              | Octo                                      |
| 9              | Nove                                      |
| 10             | Déce                                      |
| 11             | Déce-uno                                  |
| 12             | Déce-deus                                 |
| 13             | Déce-tres                                 |
| ...            | ...                                       |
| 20             | Vinti                                     |
| 30             | Trenta                                    |
| 40             | Quarenta                                  |
| 50             | Cinquenta                                 |
| 60             | Séxenta                                   |
| 70             | Septenta                                  |
| 80             | Octenta                                   |
| 90             | Noventa                                   |
| 100            | Uno cento                                 |
| 101            | Uno cento et uno                          |
| 110            | Uno cento et déce                         |
| 123            | Uno cento et vinti-tres                   |
| ...            | ...                                       |
| 1000           | Una mila                                  |
| 1000000        | Uno millio                                |
