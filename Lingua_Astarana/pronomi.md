# Pronomi (Pronouns)

|                  | Subjecto  | Accusativo | Dativo / Réflexivo | Genitivo              |
|------------------|-----------|------------|--------------------|-----------------------|
| Prima Singular   | Io        | Mé         | Me                 | Mo / Ma / Mi          |
| Secondo Singular | Tu        | Té         | Te                 | To / Ta / Ti          |
| Terzio Singular  | Ile / Ele | Lo / La    | Se                 | So / Sa / Si          |
| Prima Plurale    | Nus       | Nus        | Nus                | Nutro / Nutra / Nutri |
| Secondo Plurale  | Vus       | Vus        | Vus                | Vutro / Vutra / Vutri |
| Terzio Plurale   | Ili / Eli | Los / Las  | Se                 | So / Sa / Si          |
